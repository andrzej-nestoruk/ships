package pl.anestoruk.player

import org.scalatest._
import pl.anestoruk.models._
import pl.anestoruk.models.Ships._

class PlayerSpec extends WordSpec with Matchers{
  
  trait BaseContext {
    val playerSize = 10
  }
  
  trait EmptyPlayer extends BaseContext {
    val player = new Player("1", playerSize)
  }
  
  trait PlayerWithShips extends BaseContext {
    val ship1 = Ship(Position(0, 0), Vertical, 2)
    val ship2 = Ship(Position(2, 2), Horizontal, 3)
    val player = new Player("2", playerSize, ships = Set(ship1, ship2))
  }
  
  "Player" when {
    
    "created with empty Ships list" should {
      "contain empty `hitboxes` map" in new EmptyPlayer {
        player.hitboxes shouldBe empty
      }
    }
    
    "created with Ships" should {
      "contain proper `hitboxes` map" in new PlayerWithShips {
        val expected = Map(
            ship1 -> Set(Position(0, 1), Position(0, 0)),
            ship2 -> Set(Position(2, 2), Position(3, 2), Position(4, 2)))
            
        player.hitboxes shouldBe expected
      }
      
      "contain destroyed Ships listed within `deadShips` set" in new PlayerWithShips {
        val revealed = Seq(
            Position(0, 0), Position(0, 1), Position(0, 2), Position(2, 2))
            
        val expected = Set(ship1)
        val newPlayer = player.copy(revealed = revealed) 
        newPlayer.deadShips shouldBe expected
      }
      
      "contain damaged Ships listed within `damagedShips` set" in new PlayerWithShips {
        val revealed = Seq(
            Position(0, 0), Position(0, 1), Position(0, 2), Position(2, 2))
            
        val damagedShip = DamagedShip(Set(Position(2, 2)), None)
        val expected = Set(damagedShip)
        val newPlayer = player.copy(revealed = revealed) 
        newPlayer.damagedShips shouldBe expected
      }
      
      "contain all available Positions listd within `allPositions` set" in new {
        val playerSize = 4
        val player = Player("3", playerSize)
        val expected = Set(
            Position(0, 0), Position(1, 0), Position(2, 0), Position(3, 0),
            Position(0, 1), Position(1, 1), Position(2, 1), Position(3, 1),
            Position(0, 2), Position(1, 2), Position(2, 2), Position(3, 2), 
            Position(0, 3), Position(1, 3), Position(2, 3), Position(3, 3))
            
        player.allPositions shouldBe expected
      }
    }
    
  }
  
}