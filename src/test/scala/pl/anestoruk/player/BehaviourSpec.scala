package pl.anestoruk.player

import java.io.{ ByteArrayInputStream, ByteArrayOutputStream }
import org.scalatest._
import pl.anestoruk.models._
import pl.anestoruk.models.Ships._
import pl.anestoruk.player.Behaviour._

class BehaviourSpec extends WordSpec with Matchers{
  
  def run: String => (() => Unit) => Unit = input => f =>
    Console.withOut(new ByteArrayOutputStream) {
      Console.withIn(new ByteArrayInputStream(input.getBytes)) { f() }
    }
  
  trait Context {
    val playerSize = 10
    val ship1 = Ship(Position(0, 0), Vertical, 2)
    val ship2 = Ship(Position(2, 2), Horizontal, 3)
  }
  
  "Human" should {
    "pick Position based on user input" in new Context {
      val behaviour = Human
      val player = Player("1", playerSize, ships = Set(ship1, ship2))
      val expected = Position(5, 6)
      val input = Seq(expected.x, expected.y).mkString("\n")
      run(input){ () => 
        behaviour.pickPosition(player) shouldBe expected
      }
    }
  }
  
  "BetterAI" should {
    "pick best Position to attack using `damagedShips` and `deadShips` information" in new Context {
      val behaviour = BetterAI
      val damagedShip = DamagedShip(Set(Position(2, 2), Position(3, 2)), Some(Horizontal))
      
      val player = new {
        override val deadShips = Set(ship1)
        override val damagedShips = Set(damagedShip)
      } with Player("1", playerSize, ships = Set(ship1, ship2))
      
      val expected = Position(4, 2)
      behaviour.pickPosition(player) shouldBe expected
    }
  }
  
}