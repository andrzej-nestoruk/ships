package pl.anestoruk.player

import org.scalatest._
import pl.anestoruk.models._
import pl.anestoruk.models.Ships._
import pl.anestoruk.models.Results._
import pl.anestoruk.player.Behaviour._

class LogicSpec extends WordSpec with Matchers {
  
  trait Context {
    val playerSize = 10
    
    val ship1 = Ship(Position(1, 2), Horizontal, 4)
    val ship1Positions = Set(Position(1, 2), Position(2, 2), Position(3, 2), Position(4, 2))
    val ship2 = Ship(Position(8, 4), Vertical, 4)
    val invalidShip1 = Ship(Position(3, 2), Vertical, 2)
    
    val attacker = Player("1", playerSize, ships = Set(ship1))
    val player = Player("2", playerSize, ships = Set(ship1))
    
    val deadPlayer = new {
      override val deadShips = Set(ship1)
    } with Player("3", playerSize, ships = Set(ship1))
  }
  
  "shipPositions method" should {
    "return valid" in new Context {
      val expected = ship1Positions
      Logic.shipPositions(ship1) shouldBe expected
    }
  }
  
  "isAlive method" should {
    "return true when at least one Player's Ship is still alive" in new Context {
      Logic.isAlive(player) shouldBe true
    }
    
    "return false if all Player's Ships are destroyed" in new Context {
      Logic.isAlive(deadPlayer) shouldBe false
    }
  }
  
  "attackPosition method" should {
    "return Miss when empty Position is attacked" in new Context {
      val position = Position(0, 0)
      val attack = Attack(attacker, player, position)
      val newVictim = player.copy(revealed = player.revealed :+ position)
      val expected = AttackResult(attack.copy(victim = newVictim), Miss)
      Logic.attackPosition(attack) shouldBe expected
    }
    
    "return Hit when Position with Ship is attacked" in new Context {
      val action = Attack(attacker, player, ship1Positions.head)
      val newVictim = player.copy(revealed = player.revealed :+ ship1Positions.head)
      val expected = AttackResult(action.copy(victim = newVictim), Hit)
      Logic.attackPosition(action) shouldBe expected
    }
    
    "return HitSink when last Position from specific Ship is attacked" in new Context {
      val expected = player.copy(revealed = player.revealed ++ ship1Positions)
      val newPlayer = ship1Positions.foldLeft(player) { (newPlayer, p) => 
        val action = Attack(attacker, newPlayer, p)
        Logic.attackPosition(action).attack.victim 
      }
      newPlayer shouldBe expected
    }
  }
  
  "adjacentPositions method" should {
    "return all Positions around specified Positions" in new {
      val positions = Set(Position(5, 6), Position(5, 7))
      val expected = Set(
          Position(4, 5), Position(5, 5), Position(6, 5), 
          Position(4, 6), Position(5, 6), Position(6, 6), 
          Position(4, 7), Position(5, 7), Position(6, 7),
          Position(4, 8), Position(5, 8), Position(6, 8))
          
      Logic.adjacentPositions(positions) shouldBe expected
    }
  }
  
  "insertShip method" should {
    "insert Ship on valid Position" in new Context {
      val expected = player.copy(ships = player.ships + ship2)
      Logic.insertShip(player, ship2) shouldBe Some(expected)
    }
    
    "not insert Ship on occupied Position" in new Context {
      Logic.insertShip(player, ship1) shouldBe None
      Logic.insertShip(player, invalidShip1) shouldBe None
    }
  }
  
  "insertRandomShip method" should {
    "insert Ship on random Position, if possible" in new Context {
      val shipSize = 4
      val result = Logic.insertRandomShip(player, shipSize)
      val addedShip = result.ships.filter { _ != ship1 }.headOption
      result.ships.size shouldBe 2
      addedShip.isDefined shouldBe true
      addedShip.get.size shouldBe shipSize
    }
    
    "throw an Expection if Ship cannot be inserted" in new Context {
      val shipSize = playerSize + 1
      an [Exception] should be thrownBy Logic.insertRandomShip(player, shipSize)
    }
  }
  
}