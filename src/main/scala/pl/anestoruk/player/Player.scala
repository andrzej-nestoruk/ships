package pl.anestoruk.player

import pl.anestoruk.models._
import pl.anestoruk.models.Ships._
import pl.anestoruk.player.Behaviour._
import pl.anestoruk.player.Logic._

case class Player(
    name: String,
    size: Int,
    behaviour: Behaviour = Human,
    private[player] val ships: Set[Ship] = Set.empty,
    revealed: Seq[Position] = Seq.empty) {
  
  /**
   * Set containing all Positions that this Player has which is basically a Set of `size*size` points.
   */
  val allPositions: Set[Position] = 
    (for {
      x <- (0 to size - 1)
      y <- (0 to size - 1)
    } yield Position(x, y)).toSet
  
  /**
   * Map containing Ships and a set of Positions that each Ship occupy. 
   */
  private[player] val hitboxes: Map[Ship, Set[Position]] = ships
    .map { ship => (ship, shipPositions(ship).toSet) }
    .toMap
    
  /**
   * Set of shot-down (dead) Ships
   */
  val deadShips: Set[Ship] = ships
    .filter { hitboxes.get(_).get.forall { revealed.toSet } }
    .toSet
  
  /**
   * Set containing partial Ships that are damaged (but not shot-down totally!)
   */
  val damagedShips: Set[DamagedShip] = ships
    .filterNot { deadShips }
    .map { ship => 
      val parts = hitboxes.get(ship).get.filter { revealed.toSet }
      if (parts.size > 1)
        DamagedShip(parts, Some(ship.direction))
      else
        DamagedShip(parts, None)
    }
    .filterNot { _.positions.isEmpty }
    
  /**
   * Overriding `equals` method because we want to exclude `revealed` property from equality matching
   */
  override def equals(o: Any) = o match {
    case that: Player => 
      this.canEqual(that) && this.name == that.name && this.size == that.size && 
      this.behaviour == that.behaviour && this.ships == that.ships
    case _ => 
      false 
  }
  
  override def hashCode = (name, size, behaviour, ships).##
}