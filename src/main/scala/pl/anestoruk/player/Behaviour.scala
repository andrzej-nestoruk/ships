package pl.anestoruk.player

import scala.collection.SortedSet
import scala.io.StdIn
import scala.util.{ Random, Try }
import pl.anestoruk.models._
import pl.anestoruk.models.Ships._
import pl.anestoruk.models._
import pl.anestoruk.player.Logic._

object Behaviour {
  
  sealed trait Behaviour {
    
    /**
     * Pick Position to attack for specific enemy Player
     */
    def pickPosition(enemy: Player): Position
    
  }
  
  /**
   * Basic Human behaviour: input is taken from console
   */
  case object Human extends Behaviour {
    
    def pickPosition(enemy: Player): Position = {
      def getValidInt(msg: String, dropPredicate: Int => Boolean): Int = {
        val firstTry = Try(StdIn.readLine(msg).toInt)
        // create lazy stream that will prompt for input until user passes valid one!
        lazy val stream = Stream.continually { Try(StdIn.readLine(s"Bad input! try again! $msg").toInt) }
        (firstTry #:: stream).dropWhile { i => i.isFailure || dropPredicate(i.get) }.head.get
      }
      
      val dropPredicate: Int => Boolean = x => !(0 to enemy.size - 1).contains(x)
      val str = s"(0 to ${enemy.size - 1})"
      val x = getValidInt(s"X $str: ", dropPredicate)
      val y = getValidInt(s"Y $str: ", dropPredicate)
      Position(x, y)
    }
    
  }
  
  /**
   * Basic AI: Position is randomly picked from sequence of available Positions
   */
  case object AI extends Behaviour {
    
    def pickPosition(enemy: Player): Position = {
      val positions = enemy.allPositions.filterNot { enemy.revealed.toSet }
      positions.drop(Random.nextInt(positions.size)).head
    }
    
  }
  
  /**
   * Improved AI: ignores Positions around destroyed Ships, where other Ships can't be placed.
   * If any DamagedShips are present, BetterAI will try to finish those Ships first!
   */
  case object BetterAI extends Behaviour {
    
    def pickPosition(enemy: Player): Position = {
      // find Positions around dead ships
      val deadPositions = enemy.deadShips.map { shipPositions }.map { adjacentPositions }.flatten
        
      // filter out unwanted Positions
      val positions = enemy.allPositions.filterNot { (enemy.revealed ++ deadPositions).toSet }
      
      // prioritize Positions around damaged ships
      val bestPositions = positions.filter { 
          enemy.damagedShips.map { positionsAroundDamagedShip }.flatten
      }
      
      if (!bestPositions.isEmpty) 
        bestPositions.drop(Random.nextInt(bestPositions.size)).head
      else 
        positions.drop(Random.nextInt(positions.size)).head
    }
    
    /**
     * Find best Position(s) to attack around DamagedShip
     */
    private def positionsAroundDamagedShip(damagedShip: DamagedShip): Set[Position] = {
      val sorted = SortedSet.empty[Position] ++ damagedShip.positions
      val head = sorted.head
      val last = sorted.last
      (damagedShip.maybeDirection match {
        case Some(Horizontal) =>
          // check both horizontal ends
          Set(Position(head.x - 1, head.y), Position(last.x + 1, last.y)) 
        case Some(Vertical) =>
          // check both vertical ends
          Set(Position(head.x, head.y - 1), Position(last.x, last.y + 1)) 
        case None =>
          // if we don't know direction of the Ship, we have to check all four ends
          for  {
            x <- (-1 to 1)
            y <- (-1 to 1)
            if (Math.abs(x) != Math.abs(y))
          } yield Position(head.x + x, head.y + y)
      }).toSet
    }
    
  }
  
  /**
   * This behaviour can be used to follow fixed scenario (used in testing)
   */
  case class FollowScenario(scenario: Iterator[Position]) extends Behaviour {
    
    def pickPosition(enemy: Player): Position =
      if (scenario.hasNext)
        scenario.next
      else
        throw InvalidScenarioException
        
  }
  
}