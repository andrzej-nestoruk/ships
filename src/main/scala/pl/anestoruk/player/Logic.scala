package pl.anestoruk.player

import pl.anestoruk.models._
import pl.anestoruk.models.Results._
import pl.anestoruk.models.Ships._
import pl.anestoruk.player.Behaviour._

object Logic {
  
  /**
   * Checks if at least one Player's Ship is is not destroyed, which is required for Player to be considere 'alive'.
   */
  val isAlive: Player => Boolean = player => !player.ships.filterNot(player.deadShips).isEmpty
  
  /**
   * Transforms set of Positions into adjacent Positions. Adjacent means every Position within range of 1 field
   * from provided Position. For example (where "." are adjacent to "P"):
   * 
   * {{{
   * +---+
   * |...|
   * |.P.|
   * |...|
   * +---+
   * }}}
   * 
   */
  val adjacentPositions: Set[Position] => Set[Position] = xs =>
    (for {
      position <- xs
      x <- (-1 to 1)
      y <- (-1 to 1)
    } yield Position(position.x + x, position.y + y))
    
  /**
   * Transforms Ship into set of Positions that it is occupying
   */
  val shipPositions: Ship => Set[Position] = ship => 
    (if (ship.direction == Horizontal)
      for  {
        x <- (ship.position.x to ship.position.x + ship.size - 1)
      } yield Position(x, ship.position.y)
    else
      for {
        y <- (ship.position.y to ship.position.y + ship.size - 1) 
      } yield Position(ship.position.x, y)).toSet
      
  /**
   * Attempts to insert Ship into Player, returns updated Player on success and None of failure.
   */
  def insertShip(player: Player, ship: Ship): Option[Player] = 
    if (canInsert(player, ship)) 
      Some(player.copy(ships = player.ships + ship))
    else 
      None
      
  /**
   * Attacks specified Player's Position by updating it's `revealed` list and returning Result depending
   * on what that Attack caused to the victim.
   */
  def attackPosition(attack: Attack): AttackResult = {
    val newRevealed = attack.victim.revealed :+ attack.position
    val newVictim = attack.victim.copy(revealed = newRevealed)
    val maybeHit = attack.victim.hitboxes.find { case (_, xs) => xs.contains(attack.position) }
    val result = maybeHit match {
      case Some((ship, _)) =>
        if (!isAlive(newVictim))
          HitSinkDead
        else if (newVictim.deadShips.contains(ship))
          HitSink
        else
          Hit
      case None => 
        Miss
    }
    AttackResult(attack.copy(victim = newVictim), result)
  }
  
  /**
   * Attempts to insert random Ship ('random' affects starting position and direction of the Ship, but not size!)
   * into specified Player. Method will try to insert Ship for `attempts` number of tries and if it will not
   * succeed, it will throw an exception, so code calling this method should be wrapped with `Try`.
   * 
   * @throws Exception thrown when Ship insertion fails for `attempts` number of times
   */
  def insertRandomShip(player: Player, shipSize: Int): Player = {
    import scala.util.Random._
    val attempts = player.size * player.size * shipSize * shipSize
    val available = availablePositions(player)
    
    def error = {
      val msg = s"Couldn't fit Ship of size ${shipSize} into Player ${player.name} (${player.size}x${player.size})! " + 
        s"Player is already contaning ${player.ships.size} Ships. Number of attempts: ${attempts}. " + 
        s"Try lowering number of Ships or increasing Players' sizes to fit all elements!"
        
      val board = stringify(player, true)
      throw PlayerSetupException(msg + "\n\n" + board) 
    }
    
    Stream.continually {
      if (available.size > 0) {
        val direction = if (nextInt % 2 == 0) Horizontal else Vertical
        val position = available.drop(nextInt(available.size)).head
        val ship = Ship(position, direction, shipSize)
        insertShip(player, ship)
      } else {
        None
      }
    }
    .take(attempts).dropWhile { !_.isDefined }.headOption.flatten
    .getOrElse { error }
  }
  
  /**
   * Creates 'stringified' representation of a Player, which can be printed in console to display it's state.
   */
  def stringify(player: Player, allVisible: Boolean): String = {
    val range = (0 to player.size - 1)
    val top = "   " + range.mkString("   ") + "\n"
    val line = "  +" + Seq.fill(player.size)("---+").mkString + "\n"
    val check = (allVisible || player.behaviour == Human)
    var str = top
    for (y <- range) {
      str += line + y + " |"
      for (x <- range) {
        val position = Position(x, y)
        val field = player.hitboxes.map { _._2 }.flatten.find { _ == position }
        val revealed = player.revealed.find { _ == position }
        val result = (field, revealed) match {
          case (Some(_), Some(_)) => "[X]"
          case (None, Some(_)) => " . "
          case (Some(_), None) if check => "[ ]"
          case _ => "   "
        }
        str += result + "|"
      }
      str += "\n"
    }
    str += line
    val status = if (isAlive(player)) "" else "(DEAD)"
    
    s"${player.name} (${player.behaviour})$status: \n $str"
  }
  
  /**
   * Subtracts `hitboxes` from `allPositions` to return Set of available (insertable) Positions. 
   */
  private val availablePositions: Player => Set[Position] = player => 
    player.allPositions.filterNot { adjacentPositions(player.hitboxes.map { _._2 }.flatten.toSet) }
  
  /**
   * Checks if Ship can be inserted into Player
   */
  private val canInsert: (Player, Ship) => Boolean = (player, ship) =>
    shipPositions(ship).forall { availablePositions(player) }
  
}