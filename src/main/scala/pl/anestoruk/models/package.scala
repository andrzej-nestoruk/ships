package pl.anestoruk

import pl.anestoruk.player.Player

/**
 * Contains common models used within application
 */
package object models {
  
  case class Position(
      x: Int,
      y: Int)
  
  implicit val ord = Ordering.by { p: Position => (p.x, p.y) }
  
  object Results {
    sealed trait Result
    case object Miss extends Result
    case object Hit extends Result
    case object HitSink extends Result { override def toString = "Hit and Sink" }
    case object HitSinkDead extends Result { override def toString = "Hit, Sink and Dead" }
    
    case class Attack(
        attacker: Player, 
        victim: Player, 
        position: Position)
        
    case class AttackResult(
        attack: Attack, 
        result: Result)
  }
  
  object Ships {
    sealed trait Direction
    case object Vertical extends Direction
    case object Horizontal extends Direction
    
    case class DamagedShip(
        positions: Set[Position],
        maybeDirection: Option[Direction])
  
    case class Ship(
        position: Position,
        direction: Direction = Horizontal,
        size: Int = 1)
  }
  
  sealed abstract class CustomException(msg: String) extends Exception(msg)
  
  case class PlayerSetupException(msg: String) extends CustomException(msg)
  
  case object InvalidScenarioException extends CustomException(
      "Scenario didn't lead to Game ending state before it's iterator has run out of elements!")
}

