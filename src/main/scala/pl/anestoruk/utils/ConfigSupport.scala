package pl.anestoruk.utils

import scala.collection.JavaConverters._
import scala.util.Try
import com.typesafe.config._
trait ConfigSupport {
  
  private val config = ConfigFactory.load();  
  object Config {
    val settings = config.getConfig("settings");
    val players = config.getConfig("players");
  };    /**   * Wrapper for `getInt`, so we can provide default value in case o failure   */  def getInt(cfg: Config, key: String) =     Try(cfg.getInt(key));    /**   * Wrapper for `getString`, so we can provide default value in case o failure   */  def getStr(cfg: Config, key: String) =     Try(cfg.getString(key));    /**   * Transforms List[Number] into Seq[Int] and wraps it with Try   */  def getIntSeq(cfg: Config, key: String): Try[Seq[Int]] =     Try(cfg.getNumberList(key).asScala.map { _.intValue }.toSeq);  
}