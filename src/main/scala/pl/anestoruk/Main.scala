package pl.anestoruk

import scala.util.Try
import pl.anestoruk.core.Game
import pl.anestoruk.core.Game._
import pl.anestoruk.models._
import pl.anestoruk.player.Logic._

object Main {
  
  /**
   * Application entry point
   */
  def main(args: Array[String]): Unit = {
    
    val settings = getSettings
    
    // populate Players' boards with randomly placed Ships
    val tryPlayers = Try {
      for {
        p <- settings.initPlayers 
      } yield settings.ships.sorted.reverse.foldLeft(p) { insertRandomShip }
    }
    
    // start the game!
    tryPlayers
      .map { new Game(settings.rules).start }
      .recover { 
        case e: CustomException => println(s"\nERROR: ${e.getMessage}\n")
        case e: Throwable => throw e
      }.get
  }
  
}