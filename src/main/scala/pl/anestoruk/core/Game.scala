package pl.anestoruk.core

import pl.anestoruk.models._
import pl.anestoruk.models.Results._
import pl.anestoruk.player.Behaviour._
import pl.anestoruk.player.Logic._
import pl.anestoruk.player._
import pl.anestoruk.utils.ConfigSupport

/**
 * Holds current's Game's state
 * 
 * @param players sequence of Players within Game
 * @param attacker Player that will perform an Attack in current 'tick'
 * @param victims Players that will be attacked by attacker in current/next 'ticks'
 * @param msgs Logs and messages from the course of Game
 * @param attacks all Attacks are stored in this property
 * @param counter counts how many times `loop` was executed
 * @param finished when Game is finished it is set to true
 */
case class GameState(
    players: Seq[Player] = Seq(), 
    optAttacker: Option[Player] = None,
    victims: Iterator[Player] = Iterator.empty,
    msgs: Seq[String] = Nil, 
    attacks: Seq[Results.AttackResult] = Nil,
    counter: Int = 1,
    finished: Boolean = false)

class Game(rules: GameRule, artificialDelay: Boolean = true, maxMsgs: Int = 11) {
  
  /**
   * Rendering method - draws GameState into Console
   */
  def render(state: GameState): GameState = {
    // try this if ANSI code doesn't work!
    // println((1 to 100).map { _ => ""}.mkString("\n"))
    print("\u001b[2J" + "\u001b[H");
    
    // print Players data
    state.players.foreach { p => println(stringify(p, state.finished)) }
    
    // print messages
    println(Seq.fill(60)("*").mkString)
    state.msgs.takeRight(maxMsgs).foreach { println }
    
    // winning condition
    if (state.finished)
      println(s"\nWinner is: ${state.players.filter(isAlive).head.name}!\n")
      
    state
  }
  
  /**
   * Main game loop - performs Player's actions (Attacks), updates GameState and calls
   * `render` method which is an UI. 
   */
  def start(players: Seq[Player]): GameState = {
    def loop(initState: GameState): GameState = {
      // set attacker and victims for current 'tick'
      val state = rules.pickAttacker(initState)
      
      state.optAttacker.collect { case attacker if state.victims.hasNext =>
        val victim = state.victims.next
        
        val updatedMsgs = state.msgs :+ s"${attacker.name}'s turn to attack ${victim.name}:"
        render(state.copy(msgs = updatedMsgs))
        
        // attack Position picked by attacker
        val position = attacker.behaviour.pickPosition(victim)
        val aResult = attackPosition(Attack(attacker, victim, position))
        val attackMsg = s"${state.counter}: ${attacker.name} attacks ${victim.name} on (${position.x}, ${position.y}) -> ${aResult.result}!"
        val victimIdx = state.players.indexOf(victim)
        val updatedPlayers = state.players.updated(victimIdx, aResult.attack.victim)
        
        // When there is no Human Player in the game artificial delay can be added, so we can "see" how AIs are playing
        if (artificialDelay) 
          state.players.find { _.behaviour == Human }.getOrElse { Thread.sleep(250) }
          
        // update GameState
        val updatedState = state.copy(
            players = updatedPlayers,
            msgs = state.msgs :+ attackMsg,
            attacks = state.attacks :+ aResult,
            counter = state.counter + 1)
            
        // loop till someone wins
        if (updatedState.players.filter { isAlive }.size > 1)
          loop(updatedState)
        else 
          updatedState.copy(finished = true)
      }.getOrElse(state.copy(finished = true))
    }
    
    render(loop(GameState(players)))
  }
  
}

case class Settings(
    playersCount: Int, 
    playerSize: Int,
    ships: Seq[Int],
    initPlayers: Seq[Player],
    rules: GameRule)

/**
 * Contains pre-Game-creation method(s)
 */
object Game extends ConfigSupport {
  
  import pl.anestoruk.core.GameRules._
  
  /**
   * Reads Settings from `application.conf` or provides default values
   */
  def getSettings: Settings = {
    val playersCount = getInt(Config.settings, "playersCount")
      .getOrElse(2)
      
    val playerSize = getInt(Config.settings, "playerSize")
      .getOrElse(10)
      
    val ships = getIntSeq(Config.settings, "ships")
      .getOrElse(Seq(4, 3, 3, 2, 2, 2, 1, 1, 1, 1))
    
    val initPlayers = for {
      x <- (1 to playersCount)
    } yield {
      val player = s"p$x"
      val name = getStr(Config.players, s"$player.name")
        .getOrElse(player)
        
      val behaviour = getStr(Config.players, s"$player.behaviour")
        .getOrElse("Human").toLowerCase match {
          case "ai" => AI
          case "betterai" => BetterAI
          case _ => Human
        }
      Player(name, playerSize, behaviour)
    }
    
    val rules = getStr(Config.settings, "rules")
      .getOrElse("AttackAllEnemies").toLowerCase match {
        case "attackonenextenemy" => AttackOneNextEnemy
        case "attackallenemiestillmiss" => AttackAllEnemiesTillMiss
        case _ => AttackAllEnemies
      }
    
    Settings(playersCount, playerSize, ships, initPlayers, rules)
  }
  
}