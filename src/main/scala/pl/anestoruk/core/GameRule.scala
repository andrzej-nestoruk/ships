package pl.anestoruk.core

import scala.Iterator
import pl.anestoruk.models.Results._
import pl.anestoruk.player.Logic._

/**
 * GameRules allows to specify the flow of the game by implementing `nextAttacker` method which
 * determines next Attacker i.e. Player who will attack in upcoming round.
 */
sealed trait GameRule {
  def pickAttacker(state: GameState): GameState
}

/**
 * AttackAllEnemies means each Player will attack everyone else before next Player 
 * will be able to perform his attack(s).
 */
trait AttackAllEnemies extends GameRule {
  def pickAttacker(state: GameState) = {
    
    /**
     * Player next to current Attacker is set as a new Attacker and rest is converted into Victims Iterator
     */
    def nextAttacker = {
      val alive = state.players.filter { isAlive }
      val list = state.optAttacker.map { attacker =>
        val idx = alive.indexOf(attacker)
        val (before, after) = alive.splitAt(idx + 1)
        (after ++ before)
      }.getOrElse { alive }
      val newAttacker = list.filterNot { Set(state.optAttacker).flatten }.head
      val newVictims = list.filter { _ != newAttacker }
      state.copy(
          optAttacker = Some(newAttacker), 
          victims = newVictims.iterator)
    }
    
    if (state.victims.hasNext)
      state
    else
      nextAttacker
  }
}

/**
 * AttackOneNextEnemy means each Player will keep attacking one, neighbor (next) Player while he is alive.
 * When neighbor is dead, target is set to next available (alive) Player.
 */
trait AttackOneNextEnemy extends AttackAllEnemies() {
  override def pickAttacker(state: GameState) = { 
    val newState = super.pickAttacker(state)
    newState.copy(victims = newState.victims.take(1))
  }
}

/**
 * Extended version of AttackAllEnemies - Player can keep attacking the SAME victim until he misses his shot.
 */
trait AttackAllEnemiesTillMiss extends AttackAllEnemies {
  override def pickAttacker(state: GameState) = { 
    if (!state.attacks.isEmpty && state.attacks.last.result != Miss && isAlive(state.attacks.last.attack.victim)) {
      val newVictims = Iterator(state.attacks.last.attack.victim) ++ state.victims
      state.copy(victims = newVictims)
    } else {
      super.pickAttacker(state)
    }
  }
}

object GameRules {
  case object AttackAllEnemies extends AttackAllEnemies
  case object AttackOneNextEnemy extends AttackOneNextEnemy
  case object AttackAllEnemiesTillMiss extends AttackAllEnemiesTillMiss
}