package pl.anestoruk

import java.io.ByteArrayOutputStream
import scala.Iterator
import org.scalatest._
import pl.anestoruk.core.Game
import pl.anestoruk.core.GameRules._
import pl.anestoruk.models._
import pl.anestoruk.models.Ships._
import pl.anestoruk.player.Behaviour._
import pl.anestoruk.player.Logic._
import pl.anestoruk.player._

class IntegrationTests extends WordSpec with Matchers {
  
  def run: (() => Unit) => Unit = f => Console.withOut(new ByteArrayOutputStream) { f() }
  
  trait Context {
    val playerSize = 5
    val ship = Ship(Position(0, 0), Horizontal, 2)
    val alwaysMiss: Behaviour = FollowScenario(Iterator.continually(Position(1, 1)))
  }
  
  "Game with AttackAllEnemies rules" should {
    "end with Player1 as a winner after expected number of attacks" in new Context {
      val game = new Game(AttackAllEnemies, false) 
      
      val winningScenario: Behaviour = FollowScenario(Iterator(
          Position(0, 0), // P2 -> Hit
          Position(0, 0), // P3 -> Hit
          Position(1, 0), // P2 -> HitSinkDead
          Position(3, 3), // P3 -> Miss
          Position(1, 0)  // P3 -> HitSinkDead
      ))
      
      val player1 = Player("Player1", playerSize, winningScenario, Set(ship))
      val player2 = Player("Player2", playerSize, alwaysMiss, Set(ship))
      val player3 = Player("Player3", playerSize, alwaysMiss, Set(ship))
      val players = Seq(player1, player2, player3)
      
      run { () =>
        val result = game.start(players)
        result.finished shouldBe true
        result.attacks.size shouldBe 10 // 5x attack from Player1 + 2x attack from Player2 + 3x attack from Player3
        result.players.filter { isAlive }.head shouldBe player1
      }
    }
  }
  
  "Game with AttackOneNextEnemy rules" should {
    "end with Player2 as a winner after expected number of attacks" in new Context {
      val game = new Game(AttackOneNextEnemy, false) 
      
      val winningScenario: Behaviour = FollowScenario(Iterator(
          Position(0, 0), // P3 -> Hit
          Position(1, 1), // P3 -> Miss
          Position(1, 0), // P3 -> HitSinkDead
          Position(1, 0), // P1 -> Hit
          Position(1, 1), // P1 -> Miss
          Position(0, 0)  // P1 -> HitSinkDead
      ))
      
      val player1 = Player("Player1", playerSize, alwaysMiss, Set(ship))
      val player2 = Player("Player2", playerSize, winningScenario, Set(ship))
      val player3 = Player("Player3", playerSize, alwaysMiss, Set(ship))
      val players = Seq(player1, player2, player3)
      
      run { () =>
        val result = game.start(players)
        result.finished shouldBe true
        result.attacks.size shouldBe 14 // 6x attack from Player2 + 6x attack from Player1 + 2x attack from Player3
        result.players.filter { isAlive }.head shouldBe player2
      }
    }
  }
  
  "Game with AttackAllEnemiesTillMiss rules" should {
    "end with Player3 as a winner after expected number of attacks" in new Context {
      val game = new Game(AttackAllEnemiesTillMiss, false) 
      
      val winningScenario: Behaviour = FollowScenario(Iterator(
          Position(0, 0), // P1 -> Hit
          Position(1, 0), // P1 -> HitSinkDead
          Position(1, 0), // P2 -> Hit
          Position(0, 0)  // P2 -> HitSinkDead
      ))
      
      val player1 = Player("Player1", playerSize, alwaysMiss, Set(ship))
      val player2 = Player("Player2", playerSize, alwaysMiss, Set(ship))
      val player3 = Player("Player3", playerSize, winningScenario, Set(ship))
      val players = Seq(player1, player2, player3)
      
      run { () =>
        val result = game.start(players)
        result.finished shouldBe true
        result.attacks.size shouldBe 8 // 4x Hit from Player3 + 4x Miss from Player1 and Player2
        result.players.filter { isAlive }.head shouldBe player3
      }
    }
  }
  
  
}
