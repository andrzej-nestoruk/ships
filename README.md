# SHIPS

## Starting
Running application:

```
sbt run
```


To run tests execute:
```
sbt test
```


To run integration tests execute:

```
sbt it:test
```

---


## Gameplay

`X` - horizontal axis

`Y` - vertical axis

Ships are placed __randomly__ for each Player.

Human Input is taken from `console`.

During gameplay `console` is also used as an output channel, so it should use __fixed-width__ font to display state properly! It should look similarly to this (see [example](/example.png) for cmd.exe view):

```
Player1 (Human):
    0   1   2   3   4   5   6   7   8   9
  +---+---+---+---+---+---+---+---+---+---+
0 |   |   |   |   |   |[X]|   |   |   |   |
  +---+---+---+---+---+---+---+---+---+---+
1 |[ ]|   | . |   |   |[X]|   |[ ]|[ ]|   |
  +---+---+---+---+---+---+---+---+---+---+
2 |   |   | . |   |   |   |   |   |   |   |
  +---+---+---+---+---+---+---+---+---+---+
3 |   |   |   |   |   |   |   |   | . |   |
  +---+---+---+---+---+---+---+---+---+---+
4 |   |   |[X]|   |[ ]|   |[ ]|   |[ ]|   |
  +---+---+---+---+---+---+---+---+---+---+
5 |   |   |[X]|   |   |   |[ ]|   |[ ]|   |
  +---+---+---+---+---+---+---+---+---+---+
6 |   |   |[X]|   |   |   |[ ]|   |[ ]|   |
  +---+---+---+---+---+---+---+---+---+---+
7 |   |   |[X]|   |   |   |   |   |   |   |
  +---+---+---+---+---+---+---+---+---+---+
8 |   |   |   |   |   |[ ]|   |   |   | . |
  +---+---+---+---+---+---+---+---+---+---+
9 |   |[ ]|   |   |   |   |   |   |[X]|[X]|
  +---+---+---+---+---+---+---+---+---+---+

Player2 (BetterAI):
    0   1   2   3   4   5   6   7   8   9
  +---+---+---+---+---+---+---+---+---+---+
0 | . |   |   |   |   |   |   |   |   |   |
  +---+---+---+---+---+---+---+---+---+---+
1 |   | . |   |   |   |   |   |   |   |   |
  +---+---+---+---+---+---+---+---+---+---+
2 |   |   | . | . |   |   |   |   |   |   |
  +---+---+---+---+---+---+---+---+---+---+
3 |   |   |   |[X]| . |   |   |   |   |   |
  +---+---+---+---+---+---+---+---+---+---+
4 |   |   |   | . |   | . |   |   |   |   |
  +---+---+---+---+---+---+---+---+---+---+
5 |   |   |   |   |[X]|[X]| . |   |   |   |
  +---+---+---+---+---+---+---+---+---+---+
6 |   |   |   |   |   | . |   |   |   |   |
  +---+---+---+---+---+---+---+---+---+---+
7 |   |   |   |   |   |   |   |   |   |   |
  +---+---+---+---+---+---+---+---+---+---+
8 |   |   |   |   |   |   |   |   |   |   |
  +---+---+---+---+---+---+---+---+---+---+
9 |   |   |   |   |   |   |   |   |   |   |
  +---+---+---+---+---+---+---+---+---+---+

15: Player1 attacks Player2 on (5, 5) -> Hit!
16: Player2 attacks Player1 on (2, 6) -> Hit!
17: Player1 attacks Player2 on (5, 6) -> Miss!
18: Player2 attacks Player1 on (2, 7) -> Hit and Sink!
19: Player1 attacks Player2 on (6, 5) -> Miss!
20: Player2 attacks Player1 on (9, 9) -> Hit!
21: Player1 attacks Player2 on (5, 4) -> Miss!
22: Player2 attacks Player1 on (9, 8) -> Miss!
23: Player1 attacks Player2 on (4, 5) -> Hit!
24: Player2 attacks Player1 on (8, 9) -> Hit and Sink!
Player1's turn to attack Player2:
X (0 to 9):
```

Each field is represented by one of those symbols:

```
+---+
|   | <- field that was not shot down or is an undiscovered enemy field
+---+

+---+
| . | <- shot down field that does not contain any Ship (a.k.a. Miss)
+---+

+---+
|[ ]| <- field containing Ship (part) - visible to Human Player only
+---+

+---+
|[X]| <- shot down field that contains Ship (part)
+---+
```

Below boards representations there are `action-messages`, which provides information about what happened after each action. Each action can have 1 of 4 results: 

- `Miss` - after missed shot,
- `Hit` - after hitting a Ship,
- `Hit and Sink` - after hitting and destroying Ship,
- `Hit, Sink and Dead` - after destroying last Ship of _that_ enemy.

For example:

```
23: Player1 attacks Player2 on (4, 5) -> Hit!
24: Player2 attacks Player1 on (8, 9) -> Hit and Sink!
```

There are 3 'Game Rules' that can be used to change the flow of the Game:

- `AttackAllEnemies` - each Player attacks every other Player once and then next Player attacks everyone else,
- `AttackOneNextEnemy` - each Player attacks only his (next) neighbor. When neighbor dies, Player attacks next enemy from the list (if there are only two Players in the game this mode behaves the same way as __AttackAllEnemies__.
- `AttackAllEnemiesTillMiss` - similar to __AttackAllEnemies__, but Player can keep attacking until he misses a shot.

Game ends when there is only __one__ Player alive.

---


## Configuration

All configurable settings are located in `application.conf`.

Configurable settings include: number of Players, size of Player's board, amount (and sizes) of Ships and Game Rules.

For more details check `application.conf` comments.